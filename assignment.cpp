#include<bits/stdc++.h>
using namespace std;

#define fastio ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
#define ll long long
#define N 100005
#define M (ll)(1e9+7)

string isBalanced(string s) {
	stack<char> st;
	for (ll i = 0; i < s.length(); i++) {
		if (s[i] == '[' || s[i] == '{' || s[i] == '(') {
			st.push(s[i]);
		} else {
			if (st.empty() || (s[i] == ']' && st.top() != '[') ||
			        (s[i] == '}' && st.top() != '{') || (s[i] == '(' && st.top() != '(')) {
				return "NO";
			} else {
				st.pop();
			}
		}
	}
	if (!st.empty()) {
		return "NO";
	}
	return "YES";
}

int main() {

#ifndef ONLINE_JUDGE
	//for getting input from input.txt
	freopen("input.txt", "r", stdin);
	//for writing output to output.txt
	freopen("output.txt", "w", stdout);
#endif

	fastio;
	ll T;
	cin >> T;
	while (T--) {
		string s;
		cin >> s;
		cout << isBalanced(s) << "\n";
	}
	return 0;
}
